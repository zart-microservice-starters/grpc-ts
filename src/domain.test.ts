import { deepStrictEqual as assertEq } from "assert";
import createMessage from "./domain";

test("creates message", () => {
  assertEq(createMessage("World"), "Hello World from hellots");
});
