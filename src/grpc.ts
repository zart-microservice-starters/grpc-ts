import {
  loadPackageDefinition,
  sendUnaryData,
  Server,
  ServerCredentials,
  ServerUnaryCall,
} from "@grpc/grpc-js";
import * as Sentry from "@sentry/node";
import * as protoLoader from "@grpc/proto-loader";
import { loadConfig, setupLogger } from "./utils";
import { GreeterHandlers } from "./proto/hellots/Greeter";
import { HelloRequest } from "./proto/hellots/HelloRequest";
import { HelloReply } from "./proto/hellots/HelloReply";
import createMessage from "./domain";
import { ProtoGrpcType } from "./proto/hellots";

const greeterServer: GreeterHandlers = {
  SayHello: (
    call: ServerUnaryCall<HelloRequest, HelloReply>,
    callback: sendUnaryData<HelloReply>
  ): void => {
    callback(null, { message: createMessage(call.request.name) });
  },
};

const packageDefinition = protoLoader.loadSync("./hellots.proto");
const proto = loadPackageDefinition(
  packageDefinition
) as unknown as ProtoGrpcType;
const server = new Server();
server.addService(proto.hellots.Greeter.service, greeterServer);

async function main(): Promise<void> {
  const config = await loadConfig();
  return server.bindAsync(
    `${config.host}:${config.port}`,
    ServerCredentials.createInsecure(),
    async (err) => {
      if (err) {
        throw err;
      }
      server.start();
      Sentry.init({
        dsn: config.sentryUrl,
      });
      const logger = setupLogger(config.logLevel);
      logger.info(`Running with: Config ${JSON.stringify(config)}`);
    }
  );
}

main().then();
