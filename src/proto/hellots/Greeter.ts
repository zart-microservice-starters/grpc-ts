// Original file: hellots.proto

import type * as grpc from "@grpc/grpc-js";
import type { MethodDefinition } from "@grpc/proto-loader";
import type {
  HelloReply as _hellots_HelloReply,
  HelloReply__Output as _hellots_HelloReply__Output,
} from "../hellots/HelloReply";
import type {
  HelloRequest as _hellots_HelloRequest,
  HelloRequest__Output as _hellots_HelloRequest__Output,
} from "../hellots/HelloRequest";

export interface GreeterClient extends grpc.Client {
  SayHello(
    argument: _hellots_HelloRequest,
    metadata: grpc.Metadata,
    options: grpc.CallOptions,
    callback: grpc.requestCallback<_hellots_HelloReply__Output>
  ): grpc.ClientUnaryCall;
  SayHello(
    argument: _hellots_HelloRequest,
    metadata: grpc.Metadata,
    callback: grpc.requestCallback<_hellots_HelloReply__Output>
  ): grpc.ClientUnaryCall;
  SayHello(
    argument: _hellots_HelloRequest,
    options: grpc.CallOptions,
    callback: grpc.requestCallback<_hellots_HelloReply__Output>
  ): grpc.ClientUnaryCall;
  SayHello(
    argument: _hellots_HelloRequest,
    callback: grpc.requestCallback<_hellots_HelloReply__Output>
  ): grpc.ClientUnaryCall;
  sayHello(
    argument: _hellots_HelloRequest,
    metadata: grpc.Metadata,
    options: grpc.CallOptions,
    callback: grpc.requestCallback<_hellots_HelloReply__Output>
  ): grpc.ClientUnaryCall;
  sayHello(
    argument: _hellots_HelloRequest,
    metadata: grpc.Metadata,
    callback: grpc.requestCallback<_hellots_HelloReply__Output>
  ): grpc.ClientUnaryCall;
  sayHello(
    argument: _hellots_HelloRequest,
    options: grpc.CallOptions,
    callback: grpc.requestCallback<_hellots_HelloReply__Output>
  ): grpc.ClientUnaryCall;
  sayHello(
    argument: _hellots_HelloRequest,
    callback: grpc.requestCallback<_hellots_HelloReply__Output>
  ): grpc.ClientUnaryCall;
}

export interface GreeterHandlers extends grpc.UntypedServiceImplementation {
  SayHello: grpc.handleUnaryCall<
    _hellots_HelloRequest__Output,
    _hellots_HelloReply
  >;
}

export interface GreeterDefinition extends grpc.ServiceDefinition {
  SayHello: MethodDefinition<
    _hellots_HelloRequest,
    _hellots_HelloReply,
    _hellots_HelloRequest__Output,
    _hellots_HelloReply__Output
  >;
}
