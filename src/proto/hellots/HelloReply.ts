// Original file: hellots.proto

export interface HelloReply {
  message?: string;
}

export interface HelloReply__Output {
  message: string;
}
