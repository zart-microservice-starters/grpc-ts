import type * as grpc from "@grpc/grpc-js";
import type { MessageTypeDefinition } from "@grpc/proto-loader";

import type {
  GreeterClient as _hellots_GreeterClient,
  GreeterDefinition as _hellots_GreeterDefinition,
} from "./hellots/Greeter";

type SubtypeConstructor<
  Constructor extends new (...args: any) => any,
  Subtype
> = {
  new (...args: ConstructorParameters<Constructor>): Subtype;
};

export interface ProtoGrpcType {
  hellots: {
    Greeter: SubtypeConstructor<typeof grpc.Client, _hellots_GreeterClient> & {
      service: _hellots_GreeterDefinition;
    };
    HelloReply: MessageTypeDefinition;
    HelloRequest: MessageTypeDefinition;
  };
}
