// GENERATED CODE -- DO NOT EDIT!

"use strict";
var grpc = require("@grpc/grpc-js");
var hellots_pb = require("./hellots_pb.js");

function serialize_hellots_HelloReply(arg) {
  if (!(arg instanceof hellots_pb.HelloReply)) {
    throw new Error("Expected argument of type hellots.HelloReply");
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_hellots_HelloReply(buffer_arg) {
  return hellots_pb.HelloReply.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_hellots_HelloRequest(arg) {
  if (!(arg instanceof hellots_pb.HelloRequest)) {
    throw new Error("Expected argument of type hellots.HelloRequest");
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_hellots_HelloRequest(buffer_arg) {
  return hellots_pb.HelloRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

var GreeterService = (exports.GreeterService = {
  sayHello: {
    path: "/hellots.Greeter/SayHello",
    requestStream: false,
    responseStream: false,
    requestType: hellots_pb.HelloRequest,
    responseType: hellots_pb.HelloReply,
    requestSerialize: serialize_hellots_HelloRequest,
    requestDeserialize: deserialize_hellots_HelloRequest,
    responseSerialize: serialize_hellots_HelloReply,
    responseDeserialize: deserialize_hellots_HelloReply,
  },
});

exports.GreeterClient = grpc.makeGenericClientConstructor(GreeterService);
