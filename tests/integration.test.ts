/* eslint-disable max-lines-per-function */
import { deepStrictEqual as assertEq } from "assert";
import * as grpc from "@grpc/grpc-js";
import {
  loadPackageDefinition,
  sendUnaryData,
  Server,
  ServerUnaryCall,
} from "@grpc/grpc-js";
import promiseWrapper from "grpc-client-promise-wrapper";
import * as protoLoader from "@grpc/proto-loader";
import rpc from "./fixtures/hellots_grpc_pb";
import messages from "./fixtures/hellots_pb";
import { ProtoGrpcType } from "../src/proto/hellots";
import { GreeterHandlers } from "../src/proto/hellots/Greeter";
import { HelloRequest } from "../src/proto/hellots/HelloRequest";
import { HelloReply } from "../src/proto/hellots/HelloReply";
import createMessage from "../src/domain";

const greetServer: GreeterHandlers = {
  SayHello: (
    call: ServerUnaryCall<HelloRequest, HelloReply>,
    callback: sendUnaryData<HelloReply>
  ): void => {
    callback(null, { message: createMessage(call.request.name) });
  },
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
async function init(): Promise<[any, Server]> {
  const address = "127.0.0.1:1338";

  const packageDefinition = protoLoader.loadSync("./hellots.proto");
  const proto = loadPackageDefinition(
    packageDefinition
  ) as unknown as ProtoGrpcType;
  const server = new Server();
  server.addService(proto.hellots.Greeter.service, greetServer);
  await server.bindAsync(address, grpc.ServerCredentials.createInsecure(), () =>
    server.start()
  );

  const client = promiseWrapper(
    new rpc.GreeterClient(address, grpc.credentials.createInsecure())
  );
  return [client, server];
}

test("message response", async () => {
  const [client, server] = await init();
  const request = new messages.HelloRequest().setName("");
  const res = await client.sayHello(request);
  assertEq(res.getMessage(), "Hello undefined from hellots");
  server.forceShutdown();
});
