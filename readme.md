# grpc-ts 🌊

![](https://img.shields.io/gitlab/pipeline-status/zart-microservice-starters/grpc-ts?branch=development&logo=gitlab&style=for-the-badge)
![](https://img.shields.io/gitlab/coverage/zart-microservice-starters/grpc-ts/development?logo=jest&style=for-the-badge)
