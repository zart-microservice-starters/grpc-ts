FROM node:14.16.3-buster-slim
WORKDIR /usr/src/app
COPY package*.json ./
COPY yarn.lock ./
COPY hellots.proto ./
COPY cfg.yml ./
RUN yarn install --production
COPY dist .
ARG PORT
ARG ENV
ENV ENV=$ENV
EXPOSE $PORT
CMD [ "node", "main.js" ]

